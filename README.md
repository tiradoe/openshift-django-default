Django 1.6 and Python 3 on OpenShift
====================================

Customized version of Openshift's django quickstart.  Contains an apps directory and a config directory.  Is also set up to use mysql by default to be sure to add your database before trying to launch anything.
